#!/usr/bin/env python 

import pytest
from codewars_new_project import * 

# ensure the folder name is correct (should I ensure the system sees it or that it's just outputting correctly?)
# it must lowercase everything and outright remove special chars
# empty dates and strings not allowed

@pytest.fixture()
def foldername_creation_data():
	return [
	{'date':'20210722','string':'i am a cow','result':'20210722_i_am_a_cow'},
	{'date':'20210531','string':'You are a chicken','result':'20210531_you_are_a_chicken'},
	{'date':'20210422','string':'No I\'m not!','result':'20210422_no_im_not'},
	{'date':'20210422','string':'has-hyphen_and underscore','result':'20210422_has_hyphen_and_underscore'}
	]

def test_generate_foldername(foldername_creation_data):
	for item in foldername_creation_data:
		assert generate_foldername(item['date'],item['string']) == item['result']

def test_generate_foldername_without_string():
	assert generate_foldername('20200303') == '20200303_something_generic'

def test_create_skeleton(foldername_creation_data, tmp_path):
	project_dir = tmp_path/generate_foldername(foldername_creation_data[0]['date'],foldername_creation_data[1]['string'])
	assert not project_dir.is_dir()
	create_skeleton(project_dir)
	assert project_dir.is_dir()

	# this is supposed to ensure that the appropriate dirs/files get created 
	# In this it created /tmp/pytest-of-riley/pytest-12/test_create_skeleton0/20210722_you_are_a_chicken/ 
	# so I can see the root here

def test_cli_run():
	""" how the flip do I do that?"""
	pass