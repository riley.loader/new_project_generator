readme.md

I happen to use https://codewars.com as one method of finding challenges that improve and refine my Python skills. I hoped to do it daily but that hasn't been the case. The browser-based tools are fine but I really preferred to do everything in my editor on my desktop so I could save them locally and/or push the final results to some git repo somewhere. It's painful to recreate the files each time for each challenge, so this script was born. 

The original version (currently in 'master' branch) was very basic, but with the new version (currently in 'scrape_webpage_for_docstring' branch), it uses Selenium and BeautifulSoup to get the HTML from a fully rendered challenge page on codewars.com and parse out the pieces so that I can spend more time on solving the problem and less time setting up a mini environment in which I can solve the problem. 

Selenium could be considered a fairly steep requirement for such a simple thing, but I've also learned a thing or two along the way and in using Selenium I'm adding more tools to my belt. 