#!/usr/bin/env python

# make the folder based on today's date
# get the input (needs 1 arg, a string with the title)
# replace ' ' with '_' (in fact why am I not doing this in Python?)
# create <todaysdate>_name_of_project
# insert a 'challenge.py' and a 'notes.py' and a 'test_challenge.py' file.
# each file should have a hashbang '#!/usr/bin/env python' in it.

# this is really pretty cool. the tmp_path fixture in pytest
# https://stackoverflow.com/questions/53715059/pytest-how-to-test-project-dependent-directory-creation
# https://docs.pytest.org/en/6.2.x/tmpdir.html

from datetime import date
import re
import os
import argparse

# print(dateprefix)

def generate_foldername(folder_date='',folder_name='something generic'):
	if folder_date == '':
		folder_date = date.today().strftime("%Y%m%d")
	specialchar_regex = re.compile('[^a-z0-9_ ]') # note I want to keep the underscores and spaces at first. Why do hyphens make it through?
	newString = re.sub(specialchar_regex,'',folder_name.lower().replace(' ','_').replace('-','_')) # first do the complicated-ish regex replace. This feels so ghetto
	return f"{folder_date}_{newString}" # then return and modify on the fly for the spaces and lowercase

def create_skeleton(foldername):
	os.mkdir(foldername)
	data = "#!/usr/bin/env python\n\n"
	#  next up: write data to the files within! ezpz
	with open(f"{foldername}/challenge.py", 'w') as f:
		f.write(data) # No append function here b/c the challenge specifies its own function name
	with open(f"{foldername}/test_challenge.py",'w') as f:
		data = f"{data}import pytest\nfrom challenge import *" # I realized later that pytest needs more skeleton
		f.write(data)


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('name')
	parser.add_argument('prefix',nargs='?',default='')
	args = parser.parse_args()
	foldername = generate_foldername(args.prefix, args.name)
	create_skeleton(foldername)

if __name__ == "__main__":
	main()
